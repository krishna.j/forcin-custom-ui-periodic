import React, { useState } from "react";
import { Picker, StyleProp, View, ViewStyle } from "react-native";
import { validate } from "../validators";
export const DropDown = (initialValue: any, validators: Array<any>) => {

    const [value, setValue] = useState(initialValue);
    const [isValid, setIsValid] = useState(false);
    const [errorMesage, setErrorMesage] = useState("");

    const onChange = (itemValue: any, itemIndex: any) => {
        setValue(itemValue)
        let validator = validate(validators, itemValue)
        setIsValid(validator.isValid)
        setErrorMesage(validator.message)

    }
    const widget = (items: Array<any>, valueField: string, titleField: string, style: StyleProp<ViewStyle>) => {
        return <View style={style}>
            <Picker

                selectedValue={value}
                style={{ height: "100%", width: "100%", }}
                onValueChange={onChange}>
                {
                    items.map(
                        (item) => {
                            return <Picker.Item label={item[titleField]} value={item[valueField]} />
                        }
                    )
                }
            </Picker>
        </View>
    }
    return {
        value,
        setValue,
        onChange,
        widget,
        valid: isValid
    };
}