import { StyleProp, ViewStyle } from 'react-native';

export interface CustomProps{
    left?:any
    right?:any
    bottom?:any  
    backgroundColor?:string
    height?:any
    outputRangeFrom:number
    outputRangeTo?:number
    initialPosition?:number
    boundaryTop?:number
    customStyle?:StyleProp<ViewStyle>
    onAnimateValueChange?:any
    children:any,
    showIcon?:boolean
    dragEnabled?:boolean
}